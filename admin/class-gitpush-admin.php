<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    GitPush
 * @subpackage GitPush/admin
 */
class GitPush_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Holds any temporary validation errors
	 *
	 * @var array
	 */
	private static $errors = array();

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in GitPush_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The GitPush_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gitpush-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in GitPush_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The GitPush_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gitpush-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register API routes
	 */
	public function register_api_routes() {
		register_rest_route( GitPush::$namespace, '/deploy/', array(
			'methods' => array( 'GET', 'POST' ),
			'callback' => array( $this, 'do_deploy' )
		) );
	}

	public function do_deploy( WP_REST_Request $request ) {

		if( ! GitPush::$requirements_ok ) {
			return[ 'result' => 'Requirements not met' ];
		}

		$args = array();

		//is key valid?
		if( !$this->authenticate( $request->get_param( 'key' ) ) ) {
			return new WP_Error( 'gitpush_invalid_key', 'Invalid Key', array( 'status' => 404 ) );
		}

		//are we requesting a specific branch?
		if( $request->get_param( 'branch' ) ) {
			$args['branch'] = $request->get_param( 'branch' );
		}

		$deploy = new GitPush_Exec( GitPush::$deploy_path, $args );
		$deploy->execute();

		$result = implode( PHP_EOL, GitPush_Log::$msgs );
		update_option( 'gitpush_last_result', $result );

		return [ 'result' => $result ];
	}

	/**
	 * Check that we have a deploy key set and that it matches the provided key
	 *
	 * @param $key
	 *
	 * @return bool
	 */
	private function authenticate( $key ) {
		return GitPush::$key && GitPush::$key == $key;
	}

	public function show_admin_notices() {

		//show any deploy errors
		if( file_exists( GitPush::$error_file ) ) {
			$error = file_get_contents( GitPush::$error_file );
			$class = 'notice notice-error';
			$message = __( $error, 'gitpush' );
			printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
		}

	}

	public function add_settings_menu() {
		add_options_page( 'GitPush Settings', 'GitPush', 'manage_options', 'gitpush', array( $this, 'settings_page_render' ) );
	}

	public function settings_init() {

		register_setting( 'gitpush', 'gitpush_settings', array( $this, 'validate_settings') );

		add_settings_section(
			'gitpush_section_main',
			__( 'Main Settings', 'gitpush' ),
			array( $this, 'settings_section_render_description' ),
			'gitpush'
		);

		add_settings_field(
			'key',
			__( 'Key', 'gitpush' ),
			array( $this, 'field_key_render' ),
			'gitpush',
			'gitpush_section_main'
		);

		add_settings_field(
			'deploy_path',
			__( 'Deploy path', 'gitpush' ),
			array( $this, 'field_deploy_path_render' ),
			'gitpush',
			'gitpush_section_main'
		);

	}


	public function settings_page_render() {

		//@todo give more info here
		if( ! GitPush::$requirements_ok ) {
			echo '<h2>Requirements not met</h2>';
			return;
		}

		self::$errors = (array) get_transient( 'gitpush_settings_validation_errors' );
		delete_transient( 'gitpush_settings_validation_errors' );

		$last_result = get_option( 'gitpush_last_result' );

		?>
		<form action='options.php' method='post'>

			<h2>GitPush</h2>

			<?php
			settings_fields( 'gitpush' );
			do_settings_sections( 'gitpush' );
			submit_button();
			?>

		</form>

		<?php
		if( !empty( $last_result ) ) :
		?>
		<h2>Result from last deployment</h2>
		<pre class="deploy_last_result"><?=$last_result?></pre>
		<?php endif; ?>

		<p>Some useful info:</p>
		<p><code>git remote add origin https://user:pw@gitlab.com/user/repo.git</code><br>
		<code>git fetch --all</code></p>
		<?php

	}

	public function settings_section_render_description() {
		echo __( 'Please ensure that all settings below are correct before deploying.', 'gitpush' );
	}

	public function field_key_render() {

		$callback_url = WP_HOME . '/wp-json/' . GitPush::$namespace . '/deploy/' . '?key=' . GitPush::$key;
		$key = array_key_exists( 'key', self::$errors ) ? self::$errors['key'] : GitPush::$key;
		$style = array_key_exists( 'key', self::$errors ) ? ' style="border: 2px solid red;"' : '';
		?>
		<input type='text' class="regular-text"<?=$style?> aria-describedby="key-description" name='gitpush_settings[key]' value='<?=$key?>'>
		<p class="description" id="key-description"><?=__( 'Key to validate deploy call. Deploy callback URL should be:', 'gitpush')?><br><code><?=$callback_url?></code></p>
		<p><a href="<?=$callback_url . "&nocache=" . microtime(TRUE)?>" class="button button-secondary">Git Pull Now</a></p>
		<?php
	}


	public function field_deploy_path_render() {
		$deploy_path = array_key_exists( 'deploy_path', self::$errors ) ? self::$errors['deploy_path'] : GitPush::$deploy_path;
		$style = array_key_exists( 'deploy_path', self::$errors ) ? ' style="border: 2px solid red;"' : '';
		?>
		<input type='text' class="regular-text"<?=$style?> aria-describedby="deploy-path-description" name='gitpush_settings[deploy_path]' value='<?=$deploy_path?>'>
		<p class="description" id="deploy-path-description"><?=__( 'Path to the top level of the git repo you are deploying, where the <code>.git</code> directory is located.', 'gitpush')?></p>
		<?php
	}

	public function validate_settings( $args ) {

		$args = array_map( 'sanitize_text_field', $args );
		$errors = array();

		if( empty( $args['key'] ) ) {
			$args['key'] = GitPush::generate_new_key();
		}

		if( ! file_exists( $args['deploy_path'] ) ) {
			add_settings_error( 'gitpush', 'gitpush_error', 'Deploy path does not exist', 'error' );
			$errors['deploy_path'] = $args['deploy_path'];
		}
		if( ! empty( $args['deploy_path'] ) ) {
			$args['deploy_path'] = trailingslashit( $args['deploy_path'] );
		}
		if( empty( $errors['deploy_path'] ) && ! file_exists( $args['deploy_path'] . '.git' ) ) {
			add_settings_error( 'gitpush', 'gitpush_error', 'Deploy path is not a git repo', 'error' );
			$errors['deploy_path'] = $args['deploy_path'];
		}

		if( ! $errors ) {
			return $args;
		} else {
			//save incorrect values in transient so that when settings page is reloaded, the old values will show, but not have been saved
			set_transient( 'gitpush_settings_validation_errors', $errors, 30 );
		}
	}

	/**
	 * Strip tags & escape quotes for all user input
	 *
	 * @param $txt
	 *
	 * @return string
	 */
	public function sanitize_field( $txt ) {
		return sanitize_text_field( $txt );
	}

	/**
	 * Add settings to plugin listing page
	 * Called by plugin_action_links_{} filter
	 *
	 * @static
	 * @param $links
	 * @return array
	 */
	public static function plugin_links( $links )
	{
		$add_link = '<a href="'.get_admin_url().'options-general.php?page=gitpush">'.__('Settings').'</a>';
		array_unshift( $links, $add_link );
		return $links;
	}
}
