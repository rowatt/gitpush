=== GitPush ===
Contributors: markauk
Requires at least: 4.4
Tested up to: 4.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Here is a short description of the plugin.  This should be no more than 150 characters.  No markup here.

== Description ==

To follow...

== Installation ==

1. Upload `gitpush` directory to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Where are the FAQs? =

To follow

== Changelog ==

= 0.0 =

* Development version.
* Another change.

== Upgrade Notice ==

= 0.0 =
Upgrade notices describe the reason a user should upgrade.  No more than 300 characters.

== Arbitrary section ==

To follow...
