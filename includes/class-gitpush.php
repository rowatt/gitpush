<?php

/**
 * The file that defines the core plugin class
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    GitPush
 * @subpackage GitPush/includes
 */
class GitPush {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      GitPush_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;


	/**
	 * Key to authenticate push
	 *
	 * @since    1.0.0
	 * @var      string $key
	 */
	public static $key;

	/**
	 * Path to root of git repo we are deploying to
	 *
	 * @since    1.0.0
	 * @var     string $deploy_path
	 */
	public static $deploy_path;

	//@todo document
	public static $working_dir;

	public static $namespace = 'gitpush/v1';

	private static $min_wp_version = '4.6';
	private static $min_php_version = '7.0';
	private static $min_git_version = '2.1';

	public static $requirements_ok = TRUE;

	/**
	 * Path to file where error notifications are stored.
	 *
	 * @var string
	 */
	public static $error_file;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'gitpush';
		$this->version = '0.0.0';

		$this->check_requirements();

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gitpush-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gitpush-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-gitpush-admin.php';

		/**
		 * The class responsible for actual deployment.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gitpush-exec.php';

		/**
		 * The class responsible for logging.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gitpush-log.php';

		$this->loader = new GitPush_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the GitPush_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new GitPush_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new GitPush_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'rest_api_init', $plugin_admin, 'register_api_routes' );
		$this->loader->add_action( 'admin_notices', $plugin_admin, 'show_admin_notices' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'settings_init' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_settings_menu' );
		$this->loader->add_action( 'init', $this, 'set_options' );
		$this->loader->add_filter( 'plugin_action_links_' . GITPUSH_BASENAME, $plugin_admin, 'plugin_links' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    GitPush_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Set class variables from WP options as required
	 */
	public function set_options() {

		$this->init_options();

		$options = get_option( 'gitpush_settings' );

		//set by admin in settings page
		self::$key = $options[ 'key' ];
		self::$deploy_path = $options[ 'deploy_path' ];

		//calculated options
		self::$working_dir = WP_CONTENT_DIR . '/gitpush/';;
		self::$error_file = self::$working_dir . 'error.txt';
	}

	/**
	 * Make sure all options are properly initialised
	 */
	private function init_options() {
		$options = get_option( 'gitpush_settings' );
		$update = FALSE;

		if( empty( $options[ 'key' ] ) ) {
			$options[ 'key' ] = self::generate_new_key();
			$update = TRUE;
		}

		if( empty( $options[ 'deploy_path' ] ) ) {
			$options[ 'deploy_path' ] = ABSPATH;
			$update = TRUE;
		}

		if( $update ) {
			update_option( 'gitpush_settings', $options );
		}
	}

	/**
	 * Generate a new deployment key
	 *
	 * @return string
	 */
	public function generate_new_key() {
		return wp_generate_password( 35, false, false );
	}

	private function check_requirements() {

		if( version_compare( PHP_VERSION, self::$min_php_version, '<') ) {
			self::$requirements_ok = FALSE;
		}

		if( version_compare( get_bloginfo( 'version' ), self::$min_wp_version, '<') ) {
			self::$requirements_ok = FALSE;
		}

		$cmd = 'git --version';
		exec( $cmd, $output, $return );
		if( $return ) {
			//git not found (error 127)
			//or other git error
			self::$requirements_ok = FALSE;
		} else if( ! preg_match( '/git version ([0-9.]*)/', $output[0], $matches ) ) {
			//could not determine git version
			self::$requirements_ok = FALSE;
		} else if( version_compare( $matches[1], self::$min_git_version, '<') ) {
			self::$requirements_ok = FALSE;
		}

	}

}
