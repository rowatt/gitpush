<?php

/**
 * GitPush_Log class
 */
class GitPush_Log {

	/**
	 * The name of the file that will be used for logging deployments. Set to
	 * FALSE to disable logging.
	 *
	 * @var string
	 */
	public static $_log = FALSE;

	/**
	 * The timestamp format used for logging.
	 *
	 * @link    http://www.php.net/manual/en/function.date.php
	 * @var     string
	 */
	private static $_date_format = 'Y-m-d H:i:sP';

	/**
	 * Path to log file.
	 *
	 * Must be set before trying to do any logging.
	 *
	 * @var
	 */
	public static $log_file;

	/**
	 * Holds messages which are returned as result of API call, and stored in database
	 *
	 * @var array
	 */
	public static $msgs = array();


	/**
	 * Stores message for later use, and outputs to log file
	 *
	 * @param        $message
	 * @param string $type
	 */
	public static function msg( $message, $type = 'INFO') {

		if( 'CMD'==trim($type) ) self::$msgs[] = ''; //add spacer
		self::$msgs[] = "{$type}: {$message}";

		self::log( $message, $type );

	}

	/**
	 * Writes a message to the log file.
	 *
	 * @param  string $message The message to write
	 * @param  string $type    The type of log message (e.g. INFO, DEBUG, ERROR, etc.)
	 *
	 * @return bool
	 */
	public static function log( $message, $type = 'INFO') {

		if( ! self::$_log || ! file_exists( dirname( self::$_log ) ) ) {
			return FALSE;
		}

		// Write the message into the log file
		// Format: time --- type: message
		file_put_contents( self::$_log, date(self::$_date_format).' --- '.$type.': '.$message.PHP_EOL, FILE_APPEND );

	}

}

/* EOF */