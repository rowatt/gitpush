<?php

/**
 * Fired during plugin activation
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    GitPush
 * @subpackage GitPush/includes
 */
class GitPush_Activator {

	/**
	 * @var array list of files to create when plugin activated and contents
	 */
	public static $create_files = array(
		'.gitignore' => "*\n",
		'.htaccess' => "Deny from all\n",
		'index.php' => "<?php // Silence is golden\n",
		'README' => "All files in this directory will be deleted when plugin is deactivated.\n",
	);

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		$dir = WP_CONTENT_DIR . '/gitpush/';

		//create dir for log files etc if necessary
		if( !file_exists( $dir ) ) {
			mkdir( $dir, 0755, TRUE );
		}

		foreach( self::$create_files as $file => $contents ) {
			if( !file_exists( $dir . $file ) ) {
				file_put_contents( $dir . $file, $contents );
			}
		}

	}

}
