<?php

/**
 * GitPush_Exec class
 *
 * Runs a deployment
 *
 * Based on code from
 * http://brandonsummers.name/blog/2012/02/10/using-bitbucket-for-automated-deployments/
 *
 */
class GitPush_Exec {

	/**
	 * A callback function to call after the deploy has finished.
	 *
	 * @var callback
	 */
	public $post_deploy;

	/**
	 * The name of the branch to pull from.
	 *
	 * @var string
	 */
	private $_branch = 'master';

	/**
	 * The name of the remote to pull from.
	 *
	 * @var string
	 */
	private $_remote = 'origin';

	/**
	 * The directory where your website and git repository are located, can be
	 * a relative or absolute path
	 *
	 * @var string
	 */
	private $_directory;

	/**
	 * @var bool set to true if there was an error in the deployment
	 */
	private $exec_error = FALSE;

	/**
	 * Sets up defaults.
	 *
	 * @param  string  $directory  Directory where your website is located
	 * @param  array   $options    Information about the deployment
	 */
	public function __construct( $directory, $options = array() ) {
		// Determine the directory path
		$this->_directory = realpath( $directory ) . DIRECTORY_SEPARATOR;

		$available_options = array('branch', 'remote');
		$available_static_options = array( 'key' );

		foreach ($options as $option => $value) {
			if (in_array($option, $available_options)) {
				$this->{'_'.$option} = $value;
			}

			if (in_array($option, $available_static_options)) {
				self::${'_'.$option} = $value;
			}
		}

		if( file_exists( GitPush::$error_file ) ) {
			GitPush_Log::msg('Removing previous error file...');
			unlink( GitPush::$error_file );
	    }

		GitPush_Log::msg( 'Attempting deployment...' );
		GitPush_Log::msg( 'Start: ' . date( 'r' ) );
	}

	/**
	 * Executes the necessary commands to deploy the website.
	 *
	 * If you get a 128 error check:
	 * - permissions of .git directory
	 * - deploy path
	 *
	 * @todo capture error output with 2>&1 when there's an error
	 * @todo user specified git path
	 *
	 */
	public function execute() {
		try {
			// Make sure we're in the right directory
			chdir($this->_directory);

			// Fetch from remote (& prune obsolete branches)
			$r = $this->exec('git fetch origin --prune 2>&1', 'Fetching from origin...');
			if( $r ) {
				GitPush_Log::msg( implode( "\n", $r ), 'OUTPUT' );
			}

			//what branch are we currently on?
			$r = $this->exec('git rev-parse --abbrev-ref HEAD', 'Getting name of current branch...');
			if( !empty( $r[0] ) ) {
				if( $r[0] == $this->_branch ) {
					GitPush_Log::msg( 'On branch ' . $this->_branch );

					// we are sticking in the same branch, so
					// check if origin is different from local, if not we don't need to do any more
					$r = $this->exec( 'git log --pretty=format:"%H" -1 origin/' . $this->_branch, 'Getting most recent remote hash' );
					$origin_hash = $r[ 0 ];
					$r = $this->exec( 'git log --pretty=format:"%H" -1 ' . $this->_branch, 'Getting most recent local hash' );
					$local_hash = $r[ 0 ];

					if( $origin_hash == $local_hash ) {
						GitPush_Log::msg( 'Nothing to do - both local and remote on commit ' . $local_hash );
						return;
					}
				}
				else {
					GitPush_Log::msg( 'Switching from branch ' . $r[0] . ' to branch ' . $this->_branch );
				}
			}

			// Discard any changes to tracked files since our last deploy
			$this->exec('git reset --hard origin/' . $this->_branch, 'Reseting repository...');

			// And do the same for any submodules
			$this->exec('git submodule foreach --recursive git reset --hard', 'Reseting submodules...');

			// Make sure we are in correct branch
			$this->exec('git checkout -f ' . $this->_branch, 'Switching to branch ' . $this->_branch);

			//remove files which will be overwritten but are not yet tracked
			//@see http://stackoverflow.com/a/13242127/671189
			$cmd = sprintf('for file in `git diff HEAD..%s/%s --name-status | awk \'/^A/ {print $2}\'`; do rm -f -- "$file"; done', $this->_remote, $this->_branch );
			$this->exec($cmd, 'Ensuring no conflicts with untracked files...');

			// Update the local repository
			$this->exec('git pull '.$this->_remote.' '.$this->_branch, 'Pulling in changes...');

			// Make sure submodules get pulled in too
			$this->exec('git submodule update --init --recursive', 'Updating submodules...');

			// Make sure that local isn't ahead of origin
			$this->exec('git reset --hard origin/' . $this->_branch, 'Making sure repository is at correct commit...');

			// Secure the .git directory
			$this->exec('chmod -R og-rx .git', 'Securing .git directory...', 'ignore_errors');

			//clear the opcache if we are in PHP 5.5 or greater
			if( function_exists('opcache_reset') ) {
				opcache_reset();
			}

			if (is_callable($this->post_deploy)) {
				call_user_func($this->post_deploy);
			}

			if( $this->exec_error) {
				GitPush_Log::msg( 'Deployment failed due to an error.', 'ERROR' );
			}
			else {
				GitPush_Log::msg('Deployment successful.');
			}

		}
		catch (Exception $e) {
			$this->exec_error = TRUE;
			GitPush_Log::msg($e, 'ERROR');
		}

		if( $this->exec_error ) {
			$msg = "There was an error with the last GitPush code deployment. Please ask a site administrator to fix it.";
			file_put_contents( GitPush::$error_file, $msg );
		}

		GitPush_Log::msg( 'End: ' . date( 'r' ) );
		
	}

	/**
	 * Shell exec with logging
	 *
	 * @param string $cmd
	 * @param string $msg
	 * @param bool   $ignore_errors if true, we don't care if the command doesn't exit cleanly
	 *
	 * @return array result from command run
	 */
	private function exec( $cmd, $msg='', $ignore_errors=FALSE ) {
		GitPush_Log::msg( $cmd, 'CMD ');

		if( $this->exec_error ) {
			GitPush_Log::msg( 'Skipping due to previous error.', 'ERROR' );
			return [];
		}

		exec( $cmd, $output, $return );
		if( $return && ! $ignore_errors ) {
			$this->exec_error = TRUE;
		}

		if( $msg ) {
			GitPush_Log::msg( $msg, 'INFO');
		}

		GitPush_Log::msg( $return, 'STAT' );

		if( !empty($output) ) {
			foreach( $output as $line ) {
				GitPush_Log::msg($line, 'OUT ');
			}
		}

		if( $this->exec_error ) {
			GitPush_Log::msg( 'Aborting due to error', 'ERROR' );
		}

		return $output;
	}

}

/* EOF */