<?php

/**
 * Fired during plugin deactivation
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    GitPush
 * @subpackage GitPush/includes
 */
class GitPush_Deactivator {

	/**
	 * Clean things up when we deactivate plugin.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		$dir = WP_CONTENT_DIR . '/gitpush/';

		if( file_exists( $dir ) ) {

			//delete files we created at activation
			foreach( GitPush_Activator::$create_files as $file => $contents ) {
				unlink( $dir . $file );
			}

			//delete any other files in this dir
			array_map('unlink', glob( $dir . '*.*') );

			//remove the directory
			rmdir( $dir );
		}

		//delete options
		delete_option( 'gitpush_settings' );
	}

}
